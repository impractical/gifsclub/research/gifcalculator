package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"

	humanize "github.com/dustin/go-humanize"
	chart "github.com/wcharczuk/go-chart"
)

type bucket struct {
	kib   uint64
	count int
}

func bucketsBySize(buckets map[uint64]int) []bucket {
	b := make([]bucket, 0, len(buckets))
	for size, count := range buckets {
		b = append(b, bucket{kib: size, count: count})
	}
	sort.Slice(b, func(i, j int) bool { return b[i].kib < b[j].kib })
	return b
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("must provide cache directory")
		os.Exit(1)
	}
	fileInfos, err := ioutil.ReadDir(os.Args[1])
	if err != nil {
		panic(err)
	}
	total := uint64(0)
	dist := map[uint64]int{}
	for _, info := range fileInfos {
		contents, err := ioutil.ReadFile(filepath.Join(os.Args[1], info.Name()))
		if err != nil {
			panic(err)
		}
		num, err := strconv.Atoi(strings.TrimSpace(string(contents)))
		if err != nil {
			panic(err)
		}
		total += uint64(num)
		bucketSize := uint64(num) / 1024 / 100
		if bucketSize == 0 {
			bucketSize = 1
		}
		bucketSize = bucketSize * 1024 * 100
		bucket, err := humanize.ParseBytes(humanize.Bytes(bucketSize))
		if err != nil {
			panic(err)
		}
		dist[bucket] = dist[bucket] + 1
	}
	buckets := bucketsBySize(dist)
	for _, bucket := range buckets {
		fmt.Printf("%s,%d\n", humanize.Bytes(bucket.kib), bucket.count)
	}
	f, err := os.Create("sizes.png")
	if err != nil {
		panic(err)
	}
	err = drawChart(buckets, f)
	if err != nil {
		f.Close()
		panic(err)
	}
	f.Close()
}

func drawChart(buckets []bucket, out io.Writer) error {
	values := make([]chart.Value, 0, len(buckets))
	for _, bucket := range buckets {
		values = append(values, chart.Value{
			Value: float64(bucket.count),
			Label: humanize.Bytes(bucket.kib),
		})
	}
	pie := chart.PieChart{
		Width:  4096,
		Height: 4096,
		Values: values,
	}

	return pie.Render(chart.PNG, out)
}
